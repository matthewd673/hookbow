﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HookBow
{
    public static class Map
    {

        public static int w = 30;
        public static int h = 17;

        public static int tileSize = 64;

        static Random rng;

        public static void GenerateMap()
        {
            rng = new Random();

            int[,] map = new int[w, h];

            //place walls around the exterior
            for(int i = 0; i < w; i++)
            {
                map[i, 0] = 1;
                map[i, h - 1] = 1;
            }

            for(int j = 0; j < h; j++)
            {
                map[0, j] = 1;
                map[w - 1, j] = 1;
            }
            
            //generate some additional walls wherever
            int wallCount = rng.Next(6, 12);

            for(int i = 0; i < wallCount; i++)
            {
                map[rng.Next(w - 2) + 1, rng.Next(h - 2) + 1] = 1;
            }

            //place the monolith
            bool placedMonolith = false;

            while (!placedMonolith)
            {
                int monolithI = rng.Next(1, w);
                int monolithJ = rng.Next(1, h);
                if (map[monolithI, monolithJ] == 0)
                {
                    map[monolithI, monolithJ] = 2;
                    placedMonolith = true;
                }
            }
            //add various features as entities
            for(int i = 0; i < w; i++)
            {
                for(int j = 0; j < h; j++)
                {
                    switch (map[i, j])
                    {
                        case 1:
                            Entities.Wall newWall = new Entities.Wall(i * tileSize, j * tileSize);
                            EntityManager.AddEntity(newWall);
                            break;
                        case 2:
                            Entities.Monolith newMono = new Entities.Monolith(i * tileSize, j * tileSize);
                            EntityManager.AddEntity(newMono);
                            break;
                    }
                }
            }

        }

    }
}
