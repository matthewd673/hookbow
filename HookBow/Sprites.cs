﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HookBow
{
    public static class Sprites
    {

        public static Texture2D player;
        public static Texture2D playerSword;
        public static Texture2D hook;
        public static Texture2D enemy;
        public static Texture2D shieldEnemy;
        public static Texture2D healer;
        public static Texture2D staticShooter;
        public static Texture2D shooter;
        public static Texture2D bombCrate;
        public static Texture2D healthCrate;
        public static Texture2D pellet;
        public static Texture2D magicBolt;
        public static Texture2D monolith;

        public static Texture2D explosionMark;

        public static Texture2D blood1;
        public static Texture2D blood2;
        public static Texture2D blood3;
        public static Texture2D blood4;

        public static Texture2D[] blood;

        public static Texture2D wall;

        public static Texture2D[] walls;

        public static Texture2D rope;

        public static Texture2D cursor;
        public static Texture2D playerHealth;
        public static Texture2D playerHealthLow;

    }
}
