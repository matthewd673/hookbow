﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HookBow
{
    public static class Player
    {

        public static float x = 200;
        public static float y = 200;
        public static int w = 48;
        public static int h = 48;

        public static Texture2D sprite;

        public static int maxHealth = 300;
        public static int health = 300;
        public static int speed = 3;

        public static bool hookReady = true;
        public static bool hookCalled = false;

        static int swordRange = 8;
        static int swordDamage = 50;
        static int swingCooldown = 50;
        static int swingCt = 0;

        public static void ShootHook(int tX, int tY)
        {
            if(hookReady)
            {
                Entities.Hook hook = new Entities.Hook();
                hook.x = x;
                hook.y = y;

                Vector3 vel = Physics.CalculateVelocity(hook.x, hook.y, tX, tY);
                hook.velX = vel.X;
                hook.velY = vel.Y;
                hook.angle = vel.Z;

                hook.sprite = Sprites.hook;

                EntityManager.AddEntity(hook);

                hookReady = false;
            }
        }

        public static void CallHook()
        {
            if(!hookReady && !hookCalled)
            {
                Entities.Hook hook = new Entities.Hook();
                int hookIndex = -1;

                for(int i = 0; i < EntityManager.GetEntities().Count; i++)
                {
                    object e = EntityManager.GetEntities()[i];
                    if(e.GetType() == typeof(Entities.Hook))
                    {
                        hook = (Entities.Hook)e;
                        hookIndex = i;
                    }
                }

                hook.Call();

                if (hookIndex != -1)
                    EntityManager.ReplaceAtIndex(hookIndex, hook);

                hookCalled = true;

                swingCt = 0; //so we don't immediately swing

            }
        }

        public static void CollectHook()
        {
            hookReady = true;
            hookCalled = false;
        }

        public static void SwingSword()
        {
            if (swingCt >= swingCooldown)
            {
                swingCt = 0;
                foreach (Entity entity in EntityManager.GetEntities())
                {
                    if (entity.GetType() == typeof(Entities.Enemy))
                    {
                        Entities.Enemy e = (Entities.Enemy)entity;

                        if (Physics.DetectCollision(new Rectangle((int)x - swordRange, (int)y - swordRange, w + (swordRange * 2), h + (swordRange * 2)),
                                new Rectangle((int)e.x, (int)e.y, e.w, e.h)))
                        {
                            e.health -= swordDamage;
                        }

                    }
                }
            }
        }

        public static void OnUpdate(GameTime dt)
        {
            if (swingCt < swingCooldown)
            {
                swingCt++;
                if (swingCt >= swingCooldown)
                    sprite = Sprites.player;
                else
                    sprite = Sprites.playerSword;
            }
        }

        public static void TryMove(float newX, float newY)
        {
            bool canMove = true;

            foreach(Entity e in EntityManager.GetEntities())
            {
                if(e.GetType() == typeof(Entities.Wall))
                {
                    Entities.Wall wall = (Entities.Wall)e;

                    if(Physics.DetectCollision(new Rectangle((int)newX, (int)newY, w, h),
                        new Rectangle((int)wall.x, (int)wall.y, wall.w, wall.h)))
                    {
                        canMove = false;
                    }
                    if(Physics.DetectCollision(new Rectangle((int)x, (int)y, w, h),
                        new Rectangle((int)wall.x, (int)wall.y, wall.w, wall.h)))
                    {
                        //already colliding
                        canMove = true;
                    }
                }
            }

            if(canMove)
            {
                x = newX;
                y = newY;
            }

        }

    }
}
