﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HookBow
{
    public abstract class Entity
    {

        public float x;
        public float y;
        public int w;
        public int h;

        public Texture2D sprite;

        public abstract void OnUpdate(GameTime dt);

    }
}
