﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace HookBow
{
    public static class EntityManager
    {

        static List<Entity> entities = new List<Entity>();

        static List<Entity> toRemove = new List<Entity>();
        static List<Entity> toAdd = new List<Entity>();

        public static void AddEntity(Entity e)
        {
            toAdd.Add(e); //safer than doing it directly
        }

        public static List<Entity> GetEntities()
        {
            return entities;
        }

        public static void Remove(Entity e)
        {
            toRemove.Add(e);
        }

        public static void ReplaceAtIndex(int i, Entity e)
        {
            entities[i] = e;
        }

        public static void ClearEntities()
        {
            entities.Clear();
        }

        public static void UpdateEntities(GameTime dt)
        {
            for (int i = 0; i < entities.Count; i++)
                entities[i].OnUpdate(dt);

            //remove those scheduled
            foreach (Entity e in toRemove)
                entities.Remove(e);
            toRemove.Clear();

            //add those scheduled
            foreach (Entity e in toAdd)
                entities.Add(e);
            toAdd.Clear();

        }

    }
}
