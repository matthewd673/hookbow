﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace HookBow
{
    public static class Spawner
    {

        static int spawnWait = 100;
        static int spawnCt = 0;

        static float multiplier = 1;
        static float multIncrement = .01f;

        public static int[] baseHealth = new int[] { 20, 60, 30, 40, 30 };
        public static int[] baseSpeed = new int[] { 2, 1, 2, 0, 2 };

        static Random rng;

        public static List<Vector2> spawnPoints = new List<Vector2>();

        public static void Initialize()
        {
            rng = new Random();
        }

        public static void OnUpdate(GameTime dt)
        {

            spawnCt++;

            if (spawnCt >= spawnWait)
            {
                spawnCt = 0;

                if (spawnWait > 30) //limit how far it can scale
                {
                    spawnWait--;
                    multiplier += multIncrement;
                }

                int enemyId = 0;

                int enemyChance = rng.Next(0, 11);

                if (enemyChance < 4)
                    enemyId = 0;
                if (enemyChance >= 4 && enemyChance < 7)
                    enemyId = 1;
                if (enemyChance == 7)
                    enemyId = 2;
                if (enemyChance >= 8 && enemyChance < 10)
                    enemyId = 3;
                if (enemyChance == 10)
                    enemyId = 4;

                Entities.Enemy newEnemy = new Entities.Enemy(enemyId, baseHealth[enemyId], baseSpeed[enemyId], multiplier);
                newEnemy.x = rng.Next(Renderer.vWidth); //until we get maps in
                newEnemy.y = rng.Next(Renderer.vHeight);

                EntityManager.AddEntity(newEnemy);

                //spawn crates, rarely
                int crateId = 0;

                int crateChance = rng.Next(0, 35);

                if (crateChance == 0)
                    crateId = 0;
                else if (crateChance == 1)
                    crateId = 1;
                else
                    return;

                Entities.Crate newCrate = new Entities.Crate(crateId);
                newCrate.x = rng.Next(Renderer.vWidth);
                newCrate.y = rng.Next(Renderer.vHeight);

                EntityManager.AddEntity(newCrate);


            }
        }
    }

}
