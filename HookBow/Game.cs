﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace HookBow
{

    public class Game : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public Game()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";            
        }
        protected override void Initialize()
        {
            Window.Title = "HookBow";
            Renderer.Initialize(graphics);
            Spawner.Initialize();
            base.Initialize();

            Map.GenerateMap();
        }
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Sprites.player = Content.Load<Texture2D>("player");
            Sprites.playerSword = Content.Load<Texture2D>("player-sword");
            Sprites.hook = Content.Load<Texture2D>("hook");
            Sprites.enemy = Content.Load<Texture2D>("slime");
            Sprites.shieldEnemy = Content.Load<Texture2D>("skeleton-shield");
            Sprites.healer = Content.Load<Texture2D>("cultist-staff");
            Sprites.staticShooter = Content.Load<Texture2D>("scarecrow");
            Sprites.shooter = Content.Load<Texture2D>("cultist");
            Sprites.bombCrate = Content.Load<Texture2D>("bomb-crate");
            Sprites.healthCrate = Content.Load<Texture2D>("health-crate");
            Sprites.pellet = Content.Load<Texture2D>("pellet");
            Sprites.magicBolt = Content.Load<Texture2D>("magic-bolt");
            Sprites.monolith = Content.Load<Texture2D>("monolith");

            Sprites.explosionMark = Content.Load<Texture2D>("explosion-mark");

            Sprites.blood1 = Content.Load<Texture2D>("blood1");
            Sprites.blood2 = Content.Load<Texture2D>("blood2");
            Sprites.blood3 = Content.Load<Texture2D>("blood3");
            Sprites.blood4 = Content.Load<Texture2D>("blood4");
            Sprites.blood = new Texture2D[] { Sprites.blood1, Sprites.blood2, Sprites.blood3, Sprites.blood4 };

            Sprites.wall = Content.Load<Texture2D>("wall");
            Sprites.walls = new Texture2D[] { Sprites.wall };

            Sprites.rope = Content.Load<Texture2D>("rope");

            Sprites.cursor = Content.Load<Texture2D>("ui-cursor");
            Sprites.playerHealth = Content.Load<Texture2D>("ui-player-health");
            Sprites.playerHealthLow = Content.Load<Texture2D>("ui-player-health-low");

            //initialize fonts
            Renderer.debugFont = Content.Load<SpriteFont>("debug-font");

            //update player sprite
            Player.sprite = Sprites.player;
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            InputManager.ReadKeyboardInput(Keyboard.GetState());

            InputManager.ReadMouseInput(Mouse.GetState());

            EntityManager.UpdateEntities(gameTime);
            Spawner.OnUpdate(gameTime);
            Player.OnUpdate(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(new Color(17, 26, 51));

            Renderer.Draw(spriteBatch);

            base.Draw(gameTime);
        }
    }
}
