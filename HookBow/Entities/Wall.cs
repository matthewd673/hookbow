﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace HookBow.Entities
{
    public class Wall : Entity
    {

        Random rng;

        public Wall(float x, float y)
        {
            this.x = x;
            this.y = y;
            w = 64;
            h = 64;

            rng = new Random();

            //sprite = Sprites.walls[rng.Next(Sprites.walls.Length)];
            sprite = Sprites.wall;
        }

        public override void OnUpdate(GameTime dt)
        {
            //nothing, i think
        }

    }
}
