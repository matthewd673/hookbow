﻿using System;
using Microsoft.Xna.Framework;

namespace HookBow.Entities
{
    public class Hook : Entity
    {

        public float velX = 0;
        public float velY = 0;

        public float angle = 0;

        public static int maxSpeed = 13;
        public static int baseSpeed = 7;
        public int speed = 7;

        public static int baseDamage = 100;
        public static int damagePool = 100;

        public bool returned = false;

        public Hook()
        {
            w = 32;
            h = 32;
        }

        public void Call()
        {
            speed = baseSpeed;
        }

        public override void OnUpdate(GameTime dt)
        {

            bool shouldCallHook = false;

            //gotta get that foreach out of the way
            foreach (Entity entity in EntityManager.GetEntities())
            {

                Enemy e;
                if (entity.GetType() == typeof(Enemy))
                {
                    e = (Enemy)entity;

                    if (!Player.hookCalled && damagePool > 0)
                    {
                        //check collision and behave accordingly
                        if (Physics.DetectCollision(new Rectangle((int)x, (int)y, w, h),
                            new Rectangle((int)e.x, (int)e.y, e.w, e.h)))
                        {
                            //collided with enemy
                            if (damagePool > e.health)
                            {
                                damagePool -= e.health;
                                e.Die();
                            }
                        }
                    }

                    if (Player.hookCalled)
                    {
                        if (Physics.DetectCollision(new Rectangle((int)x, (int)y, w, h),
                                new Rectangle((int)e.x, (int)e.y, e.w, e.h)))
                        {
                            //hooking enemy
                            e.AttachToHook(this);
                        }
                    }
                }
                if (entity.GetType() == typeof(Wall))
                {
                    Wall wall = (Wall)entity;
                    if (Physics.DetectCollision(new Rectangle((int)x, (int)y, w, h),
                            new Rectangle((int)wall.x, (int)wall.y, wall.w, wall.h)))
                    {
                        //hit a wall, send it back (once its safe, of course)
                        shouldCallHook = true;
                    }
                }

            }

            if (shouldCallHook)
                Player.CallHook();

            //make sure we're headed towards the player (if hook has been called)
            if (Player.hookCalled)
            {
                Vector3 vel = Physics.CalculateVelocity(x, y, Player.x, Player.y);
                velX = vel.X;
                velY = vel.Y;
                angle = vel.Z;
                damagePool = baseDamage; //reset damage for next time
            }
            else
            {
                if (Physics.GetDistance(x, y, Player.x, Player.y) >= 680)
                    Player.CallHook();
            }

            if (speed < maxSpeed)
                speed += speed / 5;

            x = x + (velX * speed);
            y = y + (velY * speed);

            if(Physics.DetectCollision(new Rectangle((int)x, (int)y, w, h),
                new Rectangle((int)Player.x, (int)Player.y, Player.w, Player.h))) //horrible, i know
            {
                if (Player.hookCalled)
                {
                    Player.CollectHook();
                    returned = true;
                    EntityManager.Remove(this);
                }
            }

        }

    }
}
