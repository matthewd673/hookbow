﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace HookBow.Entities
{
    public class Monolith : Entity
    {

        static int maxHealth = 500;
        public int health = 500;

        public Monolith(float x, float y)
        {
            this.x = x;
            this.y = y;

            w = 48;
            h = 48;

            sprite = Sprites.monolith;
        }

        public override void OnUpdate(GameTime dt)
        {
            
        }

    }
}
