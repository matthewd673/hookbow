﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace HookBow.Entities
{
    public class Projectile : Entity
    {

        float velX;
        float velY;

        int id;

        int speed;
        int damage;

        public Projectile(int id, int damage, int speed, float x, float y, float velX, float velY)
        {
            this.id = id;

            this.speed = speed;
            this.damage = damage;

            this.x = x;
            this.y = y;

            this.velX = velX;
            this.velY = velY;

            w = 16;
            h = 16;

            switch (id)
            {
                case 0:
                    sprite = Sprites.pellet;
                    break;
                case 1:
                    sprite = Sprites.magicBolt;
                    break;
                default:
                    sprite = Sprites.pellet;
                    break;
            }
        }

        public override void OnUpdate(GameTime dt)
        {
            x += (int)(velX * speed);
            y += (int)(velY * speed);

            if(Physics.DetectCollision(new Rectangle((int)x, (int)y, w, h),
                new Rectangle((int)Player.x, (int)Player.y, Player.w, Player.h)))
            {
                //hit the player
                Player.health -= damage;
                EntityManager.Remove(this);
            }

            foreach(Entity e in EntityManager.GetEntities())
            {
                if(e.GetType() == typeof(Wall))
                {
                    Wall wall = (Wall)e;
                    if (Physics.DetectCollision(new Rectangle((int)x, (int)y, w, h),
                new Rectangle((int)wall.x, (int)wall.y, wall.w, wall.h)))
                    {
                        //hit a wall :(
                        EntityManager.Remove(this);
                    }
                }
            }

        }

    }
}
