﻿using System;
using Microsoft.Xna.Framework;

namespace HookBow.Entities
{
    public class Enemy : Entity
    {

        //enemy type ids
        public static int BASIC = 0;
        public static int SHIELD = 1;
        public static int HEALER = 2;
        public static int STATICSHOOTER = 3;
        public static int SHOOTER = 4;

        int id;

        public int health;
        int speed;

        int attackRadius = 12;
        int attackCooldown = 100;
        int attackCt = 0;
        int attackDamage = 5;

        static int baseHealerAbilityCooldown = 500;
        static int baseStaticShooterAbilityCooldown = 200;
        static int baseShooterAbilityCooldown = 300;
        int abilityCooldown = -1;
        int abilityCt = 0;

        static int healerAbilityValue = 5;
        static int staticShooterAbilityValue = 15;
        static int shooterAbilityValue = 20;

        float multiplier;

        Hook parentHook;

        Random rng;

        int targetId;
        Monolith monolith;

        public Enemy(int id, int health, int speed, float multiplier)
        {
            this.id = id;

            this.multiplier = multiplier;

            this.health = (int)(health * multiplier);
            this.speed = (int)(speed * (multiplier/2)); //multiplier affects speed significantly less

            if (this.speed < speed) //don't multiply speed to be slower
                this.speed = speed;

            w = 48;
            h = 48;

            rng = new Random();

            switch(id)
            {
                case 0:
                    sprite = Sprites.enemy;
                    attackCooldown = 200;
                    attackDamage = (int)(5 * multiplier);
                    break;
                case 1:
                    sprite = Sprites.shieldEnemy;
                    attackCooldown = 500;
                    attackDamage = (int)(20 * multiplier);
                    break;
                case 2:
                    sprite = Sprites.healer;
                    abilityCooldown = baseHealerAbilityCooldown;
                    //very weak attack on this one
                    attackCooldown = 500;
                    attackDamage = (int)(5 * multiplier);
                    break;
                case 3:
                    sprite = Sprites.staticShooter;
                    abilityCooldown = baseStaticShooterAbilityCooldown;
                    break;
                case 4:
                    sprite = Sprites.shooter;
                    abilityCooldown = baseShooterAbilityCooldown;
                    break;
            }

            targetId = rng.Next(0, 2);

            //identify monolith to target, if necessary
            if (targetId == 1)
            {
                foreach (Entity e in EntityManager.GetEntities())
                {
                    if (e.GetType() == typeof(Monolith))
                    {
                        monolith = (Monolith)e;
                    }
                }
            }

        }

        public void Die()
        {
            EntityManager.Remove(this);
            Renderer.decorations.Add(new Renderer.Decoration((int)x, (int)y, Sprites.blood[rng.Next(Sprites.blood.Length)]));

            if(id == HEALER)
            {
                Player.health += 10;
            }

        }

        public void AttachToHook(Hook hook)
        {
            parentHook = hook;
        }

        public override void OnUpdate(GameTime dt)
        {

            if (health <= 0)
            {
                Die();
                return;
            }

            //on the hook, don't act like normal
            if (parentHook != null)
            {
                x = parentHook.x;
                y = parentHook.y;
                //is hook still active?
                if(parentHook.returned)
                {
                    parentHook = null;
                }
                return;
            }

            switch (id)
            {
                case 0:
                    BasicEnemyLogic();
                    break;
                case 1:
                    BasicEnemyLogic();
                    break;
                case 2:
                    BasicEnemyLogic();
                    HealerLogic();
                    break;
                case 3:
                    //he doesn't move or attack normally
                    StaticShooterLogic();
                    break;
                case 4:
                    BasicEnemyLogic(true, false);
                    ShooterLogic();
                    break;
            }
        }

        void BasicEnemyLogic(bool moveLogic = true, bool attackLogic = true)
        {
            if (moveLogic)
            {
                //follow player
                bool canMove = true;

                //calculate new position
                Vector3 vel = new Vector3(0, 0, 0);
                if (targetId == 0)
                    vel = Physics.CalculateVelocity(x, y, Player.x, Player.y);
                if (targetId == 1)
                    vel = Physics.CalculateVelocity(x, y, monolith.x, monolith.y);
                float newX = x + (vel.X * speed);
                float newY = y + (vel.Y * speed);

                //check safety of new position before moving
                foreach (Entity e in EntityManager.GetEntities())
                {
                    if (e != this && e.GetType() != typeof(Hook)) //don't check against yourself, also you can go through hooks
                    {
                        if (Physics.DetectCollision(new Rectangle((int)newX, (int)newY, w, h),
                            new Rectangle((int)e.x, (int)e.y, e.w, e.h)))
                            canMove = false; //it will collide, can't move there

                        if (Physics.DetectCollision(new Rectangle((int)x, (int)y, w, h),
                            new Rectangle((int)e.x, (int)e.y, e.w, e.h)))
                            canMove = true; //already colliding, so why not (unless its a wall)

                    }
                }

                if (Physics.DetectCollision(new Rectangle((int)x, (int)y, w, h),
                    new Rectangle((int)Player.x, (int)Player.y, Player.w, Player.h)))
                    canMove = false; //no matter what, don't go into the player

                //move, if we're supposed to
                if (canMove)
                {
                    x = newX;
                    y = newY;
                }
            }

            if (attackLogic)
            {
                //attack
                attackCt++;

                if (attackCt >= attackCooldown)
                {
                    if (Physics.DetectCollision(new Rectangle((int)x - attackRadius, (int)y - attackRadius, w + (2 * attackRadius), h + (2 * attackRadius)),
                            new Rectangle((int)Player.x, (int)Player.y, Player.w, Player.h)))
                    {
                        attackCt = 0;
                        Player.health -= attackDamage;
                    }
                }
            }

        }

        void HealerLogic()
        {
            abilityCt++;

            if(abilityCt >= abilityCooldown)
            {
                abilityCt = 0;

                //heal all enemies
                foreach(Entity e in EntityManager.GetEntities())
                {
                    if(e.GetType() == typeof(Enemy))
                    {
                        Enemy enemy = (Enemy)e;

                        enemy.health += healerAbilityValue;
                    }
                }
            }

        }

        void StaticShooterLogic()
        {
            abilityCt++;

            if(abilityCt >= abilityCooldown && Physics.GetDistance(x, y, Player.x, Player.y) <= 800)
            {
                abilityCt = 0;

                Vector3 vel = Physics.CalculateVelocity(x, y, Player.x, Player.y);

                Projectile pellet = new Projectile(0, (int)(staticShooterAbilityValue * multiplier), 15, x, y, vel.X, vel.Y);
                EntityManager.AddEntity(pellet);
            }
        }

        void ShooterLogic()
        {
            abilityCt++;

            if(abilityCt >= abilityCooldown && Physics.GetDistance(x, y, Player.x, Player.y) <= 480)
            {
                abilityCt = 0;

                Vector3 vel = Physics.CalculateVelocity(x, y, Player.x, Player.y);

                Projectile magicBolt = new Projectile(1, (int)(shooterAbilityValue * multiplier), 10, x, y, vel.X, vel.Y);
                EntityManager.AddEntity(magicBolt);
            }
        }

    }
}
