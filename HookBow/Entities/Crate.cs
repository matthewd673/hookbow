﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace HookBow.Entities
{
    public class Crate : Entity
    {

        public static int BOMB = 0;
        public static int HEALTH = 1;

        int id;

        int explodeRadiusValue = 48;
        int explodeDamageValue = 50;
        int healValue = 50;

        public Crate(int id)
        {
            this.id = id;

            w = 48;
            h = 32;

            switch (id)
            {
                case 0:
                    sprite = Sprites.bombCrate;
                    break;
                case 1:
                    sprite = Sprites.healthCrate;
                    break;
            }

        }

        public override void OnUpdate(GameTime dt)
        {
            foreach(Entity e in EntityManager.GetEntities())
            {
                if(e.GetType() == typeof(Hook))
                {
                    Hook hook = (Hook)e;

                    if (Physics.DetectCollision(new Rectangle((int)x, (int)y, w, h),
                        new Rectangle((int)hook.x, (int)hook.y, hook.w, hook.h)))
                    {
                        //hook touched it
                        switch (id)
                        {
                            case 0:
                                Explode();
                                break;
                            case 1:
                                Heal();
                                break;
                        }
                    }
                }
            }
        }

        public void Explode()
        {
            foreach(Entity entity in EntityManager.GetEntities())
            {
                if(entity.GetType() == typeof(Enemy))
                {
                    Enemy e = (Enemy) entity;
                    
                    if(Physics.DetectCollision(new Rectangle((int)x - explodeRadiusValue, (int)y - explodeRadiusValue, w + (2 * explodeRadiusValue), h + (2 * explodeRadiusValue)),
                        new Rectangle((int)e.x, (int)e.y, e.w, e.h)))
                    {
                        e.health -= explodeDamageValue;
                    }

                }
            }
            EntityManager.Remove(this);
            Renderer.decorations.Add(new Renderer.Decoration((int)x, (int)y, Sprites.explosionMark));
        }

        public void Heal()
        {
            Player.health += healValue;
            EntityManager.Remove(this);
        }

    }
}
