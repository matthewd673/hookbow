﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace HookBow
{
    public static class InputManager
    {

        //really just for the cursor
        public static int mX;
        public static int mY;

        public static void ReadKeyboardInput(KeyboardState state)
        {
            if (state.IsKeyDown(Keys.W))
                Player.TryMove(Player.x, Player.y - Player.speed);
            if (state.IsKeyDown(Keys.S))
                Player.TryMove(Player.x, Player.y + Player.speed);
            if (state.IsKeyDown(Keys.A))
                Player.TryMove(Player.x - Player.speed, Player.y);
            if (state.IsKeyDown(Keys.D))
                Player.TryMove(Player.x + Player.speed, Player.y);

            if (state.IsKeyDown(Keys.E))
                Player.SwingSword();

        }

        public static void ReadMouseInput(MouseState state)
        {

            mX = state.X;
            mY = state.Y;

            //adjust by resolution
            float scaleX = (float)Renderer.w / Renderer.vWidth;
            float scaleY = (float)Renderer.h / Renderer.vHeight;

            mX = (int)(mX * 1/scaleX);
            mY = (int)(mY * 1/scaleY);

            if (state.LeftButton == ButtonState.Pressed)
                Player.ShootHook(mX, mY);
            if (state.RightButton == ButtonState.Pressed)
            {
                if (Player.hookReady || Player.hookCalled)
                    Player.SwingSword();
                else
                    Player.CallHook();
            }

        }

    }
}
