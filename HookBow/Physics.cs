﻿using System;
using Microsoft.Xna.Framework;

namespace HookBow
{
    public static class Physics
    {

        public static bool DetectCollision(Rectangle rect1, Rectangle rect2)
        {
            if (rect1.X < (rect2.X + rect2.Width) &&
                (rect1.X + rect1.Width) > rect2.X &&
                rect1.Y < (rect2.Y + rect2.Height) &&
                (rect1.Height + rect1.Y) > rect2.Y)
                return true;
            else
                return false;
        }

        public static Vector3 CalculateVelocity(float x, float y, float tX, float tY)
        {
            double angle = Math.Atan2(tX - x, tY - y);

            float velX = (float)Math.Sin(angle);
            float velY = (float)Math.Cos(angle);

            return new Vector3(velX, velY, (float)angle);

        }

        public static float GetDistance(float x1, float y1, float x2, float y2)
        {
            return (float)Math.Sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
        }

    }
}
