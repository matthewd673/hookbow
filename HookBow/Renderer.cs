﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HookBow
{
    public static class Renderer
    {

        public static int vWidth = 1920;
        public static int vHeight = 1080;
        public static int w = vWidth;
        public static int h = vHeight;

        public static SpriteFont debugFont;

        public static List<Decoration> decorations = new List<Decoration>();

        public static void Initialize(GraphicsDeviceManager graphics)
        {
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
            graphics.ApplyChanges();
            w = graphics.GraphicsDevice.Viewport.Width;
            h = graphics.GraphicsDevice.Viewport.Height;
        }

        public static void Draw(SpriteBatch spriteBatch)
        {

            float scaleX = (float)w / vWidth;
            float scaleY = (float)h / vHeight;
            Matrix matrix = Matrix.CreateScale(scaleX, scaleY, 1.0f);

            spriteBatch.Begin(transformMatrix: matrix, samplerState: SamplerState.PointClamp);

            //render decorations
            foreach (Decoration d in decorations)
            {
                spriteBatch.Draw(d.sprite, new Rectangle(d.x, d.y, d.sprite.Width, d.sprite.Height), Color.White);
            }

            //render entities
            foreach (Entity e in EntityManager.GetEntities())
            {
                float angle = 0f;

                if (e.GetType() == typeof(Entities.Hook))
                {
                    //calculate hook angle
                    Entities.Hook hook = (Entities.Hook)e;
                    float hookAngle = (float)Math.Sin(hook.angle);
                    if (Math.Abs(hook.angle) < 1)
                        hookAngle = 3 - hookAngle;
                    angle = hookAngle;

                    //draw rope (before drawing hook)
                    spriteBatch.DrawLine(Sprites.rope, new Vector2((Player.x + (int)(Player.w / 2)), (Player.y + (int)(Player.h / 2))), new Vector2(hook.x, hook.y));

                    Rectangle renderBounds = new Rectangle((int)e.x - (int)(e.w / 2), (int)e.y - (int)(e.h / 2), e.w, e.h); //for adjusting the origin

                    if (e.sprite != null)
                        spriteBatch.Draw(texture: e.sprite,
                            destinationRectangle: renderBounds,
                            sourceRectangle: new Rectangle(0, 0, e.sprite.Width, e.sprite.Height),
                            color: Color.White,
                            rotation: angle,
                            origin: new Vector2(e.w / 2, e.h / 2));
                }
                else
                {
                    if (e.sprite != null)
                        spriteBatch.Draw(texture: e.sprite,
                            destinationRectangle: new Rectangle((int)e.x, (int)e.y, e.w, e.h),
                            sourceRectangle: new Rectangle(0, 0, e.sprite.Width, e.sprite.Height),
                            color: Color.White);
                }

            }

            //render player
            spriteBatch.Draw(Player.sprite, new Rectangle((int)Player.x, (int)Player.y, Player.w, Player.h), Color.White); //NOT RENDERED WITH CENTER ORIGIN
            
            //ui zone
            float playerHealthPercent = (float)Player.health / (float)Player.maxHealth;
            int playerHealthWidth = (int)(Sprites.playerHealth.Width * playerHealthPercent);
            if(playerHealthPercent > .5f)
                spriteBatch.Draw(Sprites.playerHealth, new Rectangle(6, 6, playerHealthWidth, Sprites.playerHealth.Height), new Rectangle(0, 0, playerHealthWidth, Sprites.playerHealth.Height), Color.White);
            else //draw the low health one
                spriteBatch.Draw(Sprites.playerHealthLow, new Rectangle(6, 6, playerHealthWidth, Sprites.playerHealth.Height), new Rectangle(0, 0, playerHealthWidth, Sprites.playerHealth.Height), Color.White);

            spriteBatch.Draw(Sprites.cursor, new Rectangle((int)(InputManager.mX) - 4, (int)(InputManager.mY) - 4, 8, 8), Color.White); //cursor comes last

            spriteBatch.End();

        }

        public struct Decoration
        {

            public int x;
            public int y;
            public Texture2D sprite;

            public Decoration(int x, int y, Texture2D sprite)
            {
                this.x = x;
                this.y = y;
                this.sprite = sprite;
            }
        }

    }

    public static class LineRenderer
    {
        //https://gamedev.stackexchange.com/a/26027
        public static void DrawLine(this SpriteBatch spriteBatch, Texture2D texture, Vector2 start, Vector2 end)
        {
            spriteBatch.Draw(texture, start, null, Color.White,
                             (float)Math.Atan2(end.Y - start.Y, end.X - start.X),
                             new Vector2(0f, (float)texture.Height / 2),
                             new Vector2(Vector2.Distance(start, end), 1f),
                             SpriteEffects.None, 0f);
        }
    }

}
